package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
public class RunControllerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;


    @Test
    void return_find_state() {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/errors/default", String.class);
        assertEquals(HttpStatus.NOT_FOUND, forEntity.getStatusCode());
    }

    @Test
    void should_return_content() {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/errors/illegal-argument", String.class);
        assertEquals("Something wrong with the argument", forEntity.getBody());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,forEntity.getStatusCode());
    }

    @Test
    void should_handle_two_exception() {
        ResponseEntity<String> entityNull = testRestTemplate.getForEntity("/api/errors/null-pointer", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,entityNull.getStatusCode());
        assertEquals("Something wrong with the argument",entityNull.getBody());
        ResponseEntity<String>entityAir=testRestTemplate.getForEntity("/api/errors/arithmetic",String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,entityAir.getStatusCode());
        assertEquals("Something wrong with the argument",entityAir.getBody());
    }

    @Test
    void test_gloable_handle_brother() {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,forEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON,forEntity.getHeaders().getContentType());
        assertEquals("Something wrong with brother or sister.",forEntity.getBody());
    }
    @Test
    void test_gloable_handle_sister() {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(HttpStatus.I_AM_A_TEAPOT,forEntity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON,forEntity.getHeaders().getContentType());
        assertEquals("Something wrong with brother or sister.",forEntity.getBody());
    }
}
