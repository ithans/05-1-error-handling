package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.AccessControlException;

@RestController
public class RunTimeController {

    @GetMapping("/api/errors/default")
    public ResponseEntity<String> defaultMethod() {
        throw new RuntimeException("Runtime Exception");
    }

    @ExceptionHandler
    public ResponseEntity<String> runTimeHandle(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("handle" + exception.getClass().getSimpleName());
    }

    @GetMapping("/api/errors/illegal-argument")
    public ResponseEntity<String> illegalArgument() {
        throw new IllegalArgumentException("IllegalArgument Exception");
    }

    @ExceptionHandler
    public ResponseEntity<String> IllegalHandel(IllegalArgumentException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something wrong with the argument");
    }
    @GetMapping("/api/errors/null-pointer")
    public ResponseEntity<String> nullPointer(){
        throw new NullPointerException("NullPointer Exception");
    }
    @GetMapping("/api/errors/arithmetic")
    public ResponseEntity<String> airthmetic(){
        throw new ArithmeticException();
    }
    @ExceptionHandler({NullPointerException.class,ArithmeticException.class})
    public ResponseEntity<String> twoKindsHandle(){
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body("Something wrong with the argument");
    }
}
